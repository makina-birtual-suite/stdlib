
all: setup stdmem stdint stdfloat

setup:
	mkdir -p dynlibs

stdmem:
	mknb -l stdmem.mknasm
	mv stdmem.mklib dynlibs/stdmem.mklib

stdint:
	mknb -l stdint.mknasm
	mv stdint.mklib dynlibs/stdint.mklib

stdfloat:
	mknb -l stdfloat.mknasm
	mv stdfloat.mklib dynlibs/stdfloat.mklib
